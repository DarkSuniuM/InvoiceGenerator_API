from flask import Flask
from flask_restful import Api
from flask_cors import cross_origin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from config import Development

app = Flask(__name__)
app.config.from_object(Development)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)
api.decorators = [cross_origin()]
jwt = JWTManager(app)

from models import RevokedToken
from views import index
from resources import UserRegistrationEndpoint, UserLoginEndpoint, UserLogoutAccessEndpoint, UserLogoutRefreshEndpoint, \
    TokenRefreshEndpoint, InvoiceEndpoint, SingleInvoiceEndpoint, UserEditProfileEndpoint, ExportInvoiceEndpoint

api.add_resource(UserRegistrationEndpoint, '/registration')
api.add_resource(UserLoginEndpoint, '/user/login')
api.add_resource(UserEditProfileEndpoint, '/user/edit')
api.add_resource(UserLogoutAccessEndpoint, '/logout/access')
api.add_resource(UserLogoutRefreshEndpoint, '/logout/refresh')
api.add_resource(TokenRefreshEndpoint, '/token/refresh')
api.add_resource(InvoiceEndpoint, '/invoice')
api.add_resource(SingleInvoiceEndpoint, '/invoice/<int:invoice_id>')
api.add_resource(ExportInvoiceEndpoint, '/invoice/<int:invoice_id>/export')


@jwt.token_in_blacklist_loader
def check_if_token_is_blacklisted(decrypted_token):
    return RevokedToken.is_blacklisted(decrypted_token['jti'])
