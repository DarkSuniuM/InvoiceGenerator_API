from flask_restful import Resource, reqparse
from flask import request, url_for
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, \
    get_jwt_identity, get_raw_jwt
from models import db, User, RevokedToken, Invoice, InvoiceItems, InvoiceOtherCosts
from datetime import datetime as dt
from pdfkit import from_url as save_pdf


class UserRegistrationEndpoint(Resource):
    def __init__(self):
        """
        self.parser => required data
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('fullname', help='This field can not be blank', required=True)
        self.parser.add_argument('email', help='This field can not be blank', required=True)
        self.parser.add_argument('password', help='This field can not be blank', required=True)

    def post(self):
        """
        Creates a new user
        :return:
        """
        data = self.parser.parse_args()

        if User.find_by_email(data['email']):
            return {'message': 'Email in use!'}, 400

        new_user = User(email=data['email'], fullname=data['fullname'])
        new_user.hash_password(data['password'])
        db.session.add(new_user)
        access_token = create_refresh_token(identity=data['email'])
        refresh_token = create_refresh_token(identity=data['email'])
        try:
            db.session.commit()
            return {
                       'message': 'success',
                       'access_token': access_token,
                       'refresh_token': refresh_token
                   }, 201

        except Exception as e:
            db.session.rollback()
            print(str(e))
            return {'message': 'Something went wrong'}, 500


class UserLoginEndpoint(Resource):
    def __init__(self):
        """
        self.parser => required data
        """
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('email', help='This field can not be blank', required=True)
        self.parser.add_argument('password', help='This field can not be blank', required=True)

    def post(self):
        """
        Logs the user in and returns access_token + refresh_token
        :return:
        """
        data = self.parser.parse_args()
        user = db.session.query(User).filter_by(email=data['email']).first()
        if not user or not user.verify_password(data['password']):
            return {'message': 'User not found or Password wrong!'}
        else:
            access_token = create_access_token(identity=data['email'])
            refresh_token = create_refresh_token(identity=data['email'])
            return {
                'message': f'Logged in as {data["email"]}!',
                'access_token': access_token,
                'refresh_token': refresh_token
            }

    @jwt_required
    def get(self):
        user = User.query.filter_by(email=get_jwt_identity()).first()
        return {'username': user.fullname}


class UserLogoutAccessEndpoint(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()
        try:
            revoked_token = RevokedToken(jti=jti)
            revoked_token.add()
            return {'message': 'Access token had been revoked'}
        except Exception as e:
            print(str(e))
            return {'message': 'Something went wrong'}


class UserLogoutRefreshEndpoint(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()
        try:
            revoked_token = RevokedToken(jti=jti)
            revoked_token.add()
            return {'message': 'Access token had been revoked'}
        except Exception as e:
            print(str(e))
            return {'message': 'Something went wrong'}


class TokenRefreshEndpoint(Resource):
    @jwt_refresh_token_required
    def post(self):
        user = get_jwt_identity()
        access_token = create_refresh_token(identity=user)
        return {'message': 'New token generated',
                'user': user,
                'access_token': access_token}


class InvoiceEndpoint(Resource):
    @jwt_required
    def get(self):
        user = db.session.query(User).filter_by(email=get_jwt_identity()).first()
        user_invoices = []
        for invoice in user.invoices:
            user_invoices.append({
                'invoice_id': invoice.id,
                'created_at': str(invoice.created_date),
                'export': {
                    'pdf': url_for('static', filename=f'exports/Invoice_{invoice.id}.pdf', _external=True)
                },
                'invoice': {
                    'number': invoice.number if invoice.number else None,
                    'date': str(invoice.date),
                    'terms': invoice.terms if invoice.terms else None,
                    'company_name': invoice.company_name if invoice.company_name else None,
                    'company_logo': str(invoice.company_logo, 'UTF-8') if invoice.company_logo else None,
                    'company_email': invoice.company_email if invoice.company_email else None,
                    'company_address': invoice.company_address if invoice.company_address else None,
                    'company_telephone': invoice.company_telephone if invoice.company_telephone else None,
                    'company_website': invoice.company_website if invoice.company_website else None,
                    'customer_name': invoice.customer_name if invoice.customer_name else None,
                    'customer_email': invoice.customer_email if invoice.customer_email else None,
                    'billing_expenses': invoice.billing_expenses if invoice.billing_expenses else None,
                    'payment_instructions': invoice.payment_instructions if invoice.payment_instructions else None,
                    'comments': invoice.comments if invoice.comments else None,
                    'currency': invoice.currency if invoice.currency else None,
                    'currency_pos': invoice.currency_pos if invoice.currency_pos else None,
                    'discount': invoice.discount if invoice.discount else None,
                    'discount_type': invoice.discount_type if invoice.discount_type else None
                    ,
                    'items': [
                        {
                            'name': item.name,
                            'description': item.description,
                            'price': item.price,
                            'quantity': item.quantity
                        } for item in invoice.items],
                    'other_costs': [
                        {
                            'description': other_cost.description,
                            'type': other_cost.type,
                            'price_or_percent': other_cost.price_or_percent
                        } for other_cost in invoice.other_costs
                    ]
                }})
        return {'message': f'All invoices created by {user.fullname}',
                'invoices': list(user_invoices)}

    @jwt_required
    def post(self):

        # data = self.parse_args(request.json)
        data = {}
        required_data = ['date', 'company_name', 'customer_name', 'currency', 'items', 'currency_pos']
        for each in required_data:
            if not each in request.json.keys():
                return {'message': {each: 'This field can not be blank'}}, 400
            if request.json[each] is None:
                return {'message': {each: 'This field can not be blank'}}, 400
            data[each] = request.json[each]
        optional_data = ['number', 'terms', 'company_logo', 'company_email', 'company_telephone', 'company_address',
                         'company_website', 'customer_email', 'billing_expenses', 'payment_instructions', 'comments',
                         'discount', 'discount_type', 'other_costs']
        for each in optional_data:
            data[each] = request.json[each] if each in request.json.keys() else None
        user = db.session.query(User).filter_by(email=get_jwt_identity()).first()

        # Processing Data Items and Other Costs START
        items = [InvoiceItems(**item) for item in data['items']]
        if data['other_costs']:
            other_costs = [InvoiceOtherCosts(**other_cost) for other_cost in data['other_costs']]
        else:
            other_costs = None
        # Processing Data Items and Other Costs END

        new_invoice = Invoice(
            date=dt.strptime('2018-01-01', '%Y-%m-%d'),
            number=int(data['number']) if data['number'] else None,
            terms=data['terms'] if data['terms'] else None,
            company_name=data['company_name'] if data['company_name'] else None,
            company_logo=bytes(data['company_logo'], 'UTF-8') if data['company_logo'] else None,
            company_email=data['company_email'] if data['company_email'] else None,
            company_address=data['company_address'] if data['company_address'] else None,
            company_telephone=data['company_telephone'] if data['company_telephone'] else None,
            company_website=data['company_website'] if data['company_website'] else None,
            customer_name=data['customer_name'] if data['customer_name'] else None,
            customer_email=data['customer_email'] if data['customer_email'] else None,
            billing_expenses=data['billing_expenses'] if data['billing_expenses'] else None,
            payment_instructions=data['payment_instructions'] if data['payment_instructions'] else None,
            comments=data['comments'] if data['comments'] else None,
            currency=data['currency'] if data['currency'] else None,
            currency_pos=data['currency_pos'] if data['currency_pos'] else None,
            discount=data['discount'] if data['discount'] else None,
            discount_type=data['discount_type'] if data['discount_type'] else None,
            user_id=user.id
        )

        for item in items:
            new_invoice.items.append(item)
        if other_costs:
            for other_cost in other_costs:
                new_invoice.other_costs.append(other_cost)
        db.session.add(new_invoice)
        try:
            db.session.commit()
            save_pdf(f'http://127.0.0.1:5000/export/{new_invoice.md5}', f'./static/exports/Invoice_{new_invoice.id}.pdf')
        except Exception as e:
            print(str(e))
            db.session.rollback()
            return {'message': 'Something went wrong'}, 500

        return {'message': 'success', 'id': new_invoice.id}


class SingleInvoiceEndpoint(Resource):
    @jwt_required
    def get(self, invoice_id):
        user = User.query.filter_by(email=get_jwt_identity()).first()
        invoice = Invoice.query.filter_by(user_id=user.id, id=invoice_id).first()
        if invoice:
            return {'message': 'success',
                    'invoice_meta': {
                        'id': invoice_id,
                        'created_at': str(invoice.created_date),
                        'export': {
                            'pdf': url_for('static', filename=f'exports/Invoice_{invoice.id}.pdf', _external=True)
                        }
                    },
                    'invoice': {
                        'number': invoice.number,
                        'date': str(invoice.date),
                        'terms': invoice.terms,
                        'company_name': invoice.company_name,
                        'company_logo': str(invoice.company_logo, 'UTF-8') if invoice.company_logo else None,
                        'company_email': invoice.company_email,
                        'company_address': invoice.company_address,
                        'company_telephone': invoice.company_telephone,
                        'company_website': invoice.company_website,
                        'customer_name': invoice.customer_name,
                        'customer_email': invoice.customer_email,
                        'billing_expenses': invoice.billing_expenses,
                        'payment_instructions': invoice.payment_instructions,
                        'comments': invoice.comments,
                        'currency': invoice.currency,
                        'currency_pos': invoice.currency_pos,
                        'discount': invoice.discount,
                        'discount_type': invoice.discount_type,
                        'items': [
                            {
                                'name': item.name,
                                'description': item.description,
                                'price': item.price,
                                'quantity': item.quantity
                            } for item in invoice.items],
                        'other_costs': [
                            {
                                'description': other_cost.description,
                                'type': other_cost.type,
                                'price_or_percent': other_cost.price_or_percent
                            } for other_cost in invoice.other_costs
                        ]
                    }}
        return {'message': 'Invoice not found or You are not authorized to this invoice'}, 400

    @jwt_required
    def put(self, invoice_id):
        user = User.query.filter_by(email=get_jwt_identity()).first()
        invoice = Invoice.query.filter_by(user_id=user.id, id=invoice_id).first()
        if invoice:  # I hard coded this part, PR if u think u can help me
            # data = InvoiceEndpoint.parse_args(request.json)
            data = {}
            required_data = ['date', 'company_name', 'customer_name', 'currency', 'items', 'currency_pos']
            for each in required_data:
                if not each in request.json.keys():
                    return {'message': {each: 'This field can not be blank'}}, 400
                if request.json[each] is None:
                    return {'message': {each: 'This field can not be blank'}}, 400
                data[each] = request.json[each]
            optional_data = ['number', 'terms', 'company_logo', 'company_email', 'company_telephone', 'company_address',
                             'company_website', 'customer_email', 'billing_expenses', 'payment_instructions',
                             'comments',
                             'discount', 'discount_type', 'other_costs']
            for each in optional_data:
                data[each] = request.json[each] if each in request.json.keys() else None
            invoice.number = data['number']
            invoice.terms = data['terms']
            invoice.company_logo = bytes(data['company_logo'], 'utf-8') if data['company_logo'] else None
            invoice.company_email = data['company_email']
            invoice.company_telephone = data['company_telephone']
            invoice.company_address = data['company_address']
            invoice.company_website = data['company_website']
            invoice.customer_email = data['customer_email']
            invoice.billing_expenses = data['billing_expenses']
            invoice.payment_instructions = data['payment_instructions']
            invoice.comments = data['comments']
            invoice.discount = data['discount']
            invoice.discount_type = data['discount_type']
            invoice.date = data['date']
            invoice.company_name = data['company_name']
            invoice.customer_name = data['customer_name']
            invoice.currency = data['currency']
            invoice.items = [InvoiceItems(**item) for item in data['items']]
            if data['other_costs']:
                invoice.other_costs = [InvoiceOtherCosts(**other_cost) for other_cost in data['other_costs']]
            try:
                db.session.commit()
            except Exception as e:
                print(str(e))
                db.session.rollback()
                return {'message': 'Something went wrong!'}, 400
            return {'message': f'Invoice #{invoice.id} Updated!'}, 201

        return {'message': 'Invoice not found or You are not authorized to this invoice'}, 400

    @jwt_required
    def delete(self, invoice_id):
        user = User.query.filter_by(email=get_jwt_identity()).first()
        invoice = Invoice.query.filter_by(user_id=user.id, id=invoice_id).first()
        if invoice:
            Invoice.query.filter_by(id=invoice.id).delete()
            try:
                db.session.commit()
                save_pdf(f'http://127.0.0.1:5000/export/{invoice.md5}', f'./static/exports/Invoice_{invoice.id}.pdf')
                return {'message': f'Invoice #{invoice.id} Deteled!'}, 201
            except Exception as e:
                print(str(e))
                db.session.rollback()
                return {'message': 'Somewthing went wrong'}, 400
        return {'message': 'Invoice not found or You are not authorized to this invoice'}, 400


class UserEditProfileEndpoint(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('fullname', help='This field can not be blank', required=True)
        self.parser.add_argument('password', help='This field can not be blank', required=False)

    @jwt_required
    def put(self):
        user = User.query.filter_by(email=get_jwt_identity()).first()
        data = self.parser.parse_args()
        user.fullname = data['fullname']
        user.password = data['password'] if data['password'] else user.password
        try:
            db.session.commit()
            return {'message': f'Your profile has been updated.'}, 201
        except Exception as e:
            print(str(e))
            db.session.rollback()
            return {'message': 'Something went wrong'}, 400


class ExportInvoiceEndpoint(Resource):
    @jwt_required
    def get(self, invoice_id):
        invoice = Invoice.query.get(invoice_id)
        save_pdf(f'http://127.0.0.1:5000/export/{invoice.md5}', f'./static/exports/Invoice_{invoice.id}.pdf')
        return {'message': 'Here is the link',
                'formats': {
                    'pdf': url_for('static', filename=f'exports/Invoice_{invoice.id}.pdf', _external=True)
                }}
