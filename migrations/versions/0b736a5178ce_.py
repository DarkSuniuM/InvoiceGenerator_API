"""empty message

Revision ID: 0b736a5178ce
Revises: 5cfe085cb7df
Create Date: 2018-05-27 05:40:37.130790

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '0b736a5178ce'
down_revision = '5cfe085cb7df'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('invoice_items', sa.Column('invoice_id', sa.Integer(), nullable=True))
    op.drop_constraint('invoice_items_ibfk_1', 'invoice_items', type_='foreignkey')
    op.create_foreign_key(None, 'invoice_items', 'invoice', ['invoice_id'], ['id'])
    op.drop_column('invoice_items', 'invoice')
    op.add_column('invoice_other_costs', sa.Column('invoice_id', sa.Integer(), nullable=True))
    op.drop_constraint('invoice_other_costs_ibfk_1', 'invoice_other_costs', type_='foreignkey')
    op.create_foreign_key(None, 'invoice_other_costs', 'invoice', ['invoice_id'], ['id'])
    op.drop_column('invoice_other_costs', 'invoice')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('invoice_other_costs', sa.Column('invoice', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'invoice_other_costs', type_='foreignkey')
    op.create_foreign_key('invoice_other_costs_ibfk_1', 'invoice_other_costs', 'invoice', ['invoice'], ['id'])
    op.drop_column('invoice_other_costs', 'invoice_id')
    op.add_column('invoice_items', sa.Column('invoice', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'invoice_items', type_='foreignkey')
    op.create_foreign_key('invoice_items_ibfk_1', 'invoice_items', 'invoice', ['invoice'], ['id'])
    op.drop_column('invoice_items', 'invoice_id')
    # ### end Alembic commands ###
