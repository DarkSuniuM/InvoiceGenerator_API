"""empty message

Revision ID: ed786f340cf3
Revises: e6aaf4af9f80
Create Date: 2018-06-14 20:43:28.227815

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'ed786f340cf3'
down_revision = 'e6aaf4af9f80'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('invoice', 'company_logo')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('invoice', sa.Column('company_logo', mysql.VARCHAR(length=128), nullable=True))
    # ### end Alembic commands ###
