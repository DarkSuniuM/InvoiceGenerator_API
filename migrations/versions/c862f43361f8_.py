"""empty message

Revision ID: c862f43361f8
Revises: 49b66eccdc12
Create Date: 2018-06-18 00:21:59.974097

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c862f43361f8'
down_revision = '49b66eccdc12'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('invoice', sa.Column('md5', sa.String(length=128), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('invoice', 'md5')
    # ### end Alembic commands ###
