from flask import jsonify, render_template
from app import app
from models import Invoice


@app.route('/')
def index():
    return jsonify(message='Hello, World')


@app.route('/export/<string:md5_id>')
def invoice_html(md5_id):
    invoice = Invoice.query.filter_by(md5=md5_id).first()
    logo = str(invoice.company_logo, 'UTF-8') if invoice.company_logo else None
    return render_template('invoice.html', invoice=invoice, company_logo=logo)
