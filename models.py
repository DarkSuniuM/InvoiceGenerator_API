from app import db
from datetime import datetime
from passlib.apps import custom_app_context as pwd_context
from passlib.hash import ldap_hex_md5
from random import randint


class User(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(128), nullable=False, unique=True)
    fullname = db.Column(db.String(64), nullable=False, unique=False)
    password = db.Column(db.String(128), nullable=False, unique=False)
    invoices = db.relationship('Invoice', backref='user', lazy='dynamic', cascade="all, delete-orphan")

    def __init__(self, email, fullname):
        self.email = email
        self.fullname = fullname

    def hash_password(self, password):
        self.password = pwd_context.encrypt(str(password))

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def return_all(cls):
        def to_json(user):
            return {
                'id': user.id,
                'email': user.email,
                'fullName': user.fullname
            }

        return {'users': list(map(lambda user: to_json(user), User.query.all()))}

    @classmethod
    def delete_all(cls):
        num_deleted_rows = db.session.query(cls).delete()
        try:
            db.session.commit()
            return {'message': f'{num_deleted_rows} row(s) dropped!'}
        except Exception as e:
            db.session.rollback()
            print(str(e))
            return {'message', 'Something went wrong!'}


class RevokedToken(db.Model):
    __tablename__ = "revoked_tokens"
    id = db.Column(db.Integer(), primary_key=True)
    jti = db.Column(db.String(120), nullable=False, unique=True)

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_blacklisted(cls, jti):
        return bool(cls.query.filter_by(jti=jti).first())


class Invoice(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    date = db.Column(db.Date(), nullable=False, unique=False)
    number = db.Column(db.Integer(), nullable=True, unique=False)
    terms = db.Column(db.Text(1024), nullable=True, unique=False)
    company_name = db.Column(db.String(128), nullable=False, unique=False)
    company_logo = db.Column(db.BLOB(), nullable=True, unique=False)
    company_email = db.Column(db.String(128), nullable=True, unique=False)
    company_address = db.Column(db.String(256), nullable=True, unique=False)
    company_telephone = db.Column(db.String(16), nullable=True, unique=False)
    company_website = db.Column(db.String(128), nullable=True, unique=False)
    customer_name = db.Column(db.String(128), nullable=False, unique=False)
    customer_email = db.Column(db.String(128), nullable=True, unique=False)
    items = db.relationship('InvoiceItems', backref='invoice', lazy='dynamic')
    billing_expenses = db.Column(db.Text(1024), nullable=True, unique=False)
    other_costs = db.relationship('InvoiceOtherCosts', backref='invoice', cascade='all, delete', lazy='dynamic')
    payment_instructions = db.Column(db.Text(1024), nullable=True, unique=False)
    comments = db.Column(db.Text(1024), nullable=True, unique=False)
    currency = db.Column(db.String(16), nullable=False, unique=False)
    currency_pos = db.Column(db.Enum('Left', 'Right'))
    discount = db.Column(db.Float(), nullable=True, unique=False)
    discount_type = db.Column(db.Enum('Percentage', 'Fixed'), nullable=True, unique=False)
    created_date = db.Column(db.Date(), default=datetime.utcnow().date())
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    md5 = db.Column(db.String(128), nullable=False)

    def __init__(self, date, number, terms, company_name, company_logo, company_email, company_address,
                 company_telephone, company_website, customer_name, customer_email, billing_expenses,
                 payment_instructions, comments, currency, currency_pos, discount, discount_type, user_id):
        self.date = date
        self.number = number
        self.terms = terms
        self.company_name = company_name
        self.company_logo = company_logo
        self.company_email = company_email
        self.company_address = company_address
        self.company_telephone = company_telephone
        self.company_website = company_website
        self.customer_name = customer_name
        self.customer_email = customer_email
        self.billing_expenses = billing_expenses
        self.payment_instructions = payment_instructions
        self.comments = comments
        self.currency = currency
        self.currency_pos = currency_pos
        self.discount = discount
        self.discount_type = discount_type
        self.user_id = user_id
        self.md5 = ldap_hex_md5.hash(str(self.id) + "_generated_" + str(self.user_id) + "_" + str(randint(1000, 9999)))[5:]

    def total_price(self):
        total = 0
        percent = 0
        for item in self.items:
            total += item.price
        for other_cost in self.other_costs:
            if other_cost.type == 'Fixed':
                total += other_cost.price_or_percent
            else:
                percent += other_cost.price_or_percent
        total = total + (total * percent / 100)
        return total


class InvoiceItems(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    invoice_id = db.Column(db.Integer(), db.ForeignKey('invoice.id', ondelete='CASCADE'))
    name = db.Column(db.String(256), nullable=False, unique=False)
    description = db.Column(db.Text(1024), nullable=True, unique=False)
    price = db.Column(db.Float(), nullable=False, unique=False)
    quantity = db.Column(db.Integer(), nullable=False, unique=False)


class InvoiceOtherCosts(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    invoice_id = db.Column(db.Integer(), db.ForeignKey('invoice.id', ondelete='CASCADE'))
    description = db.Column(db.String(256), nullable=False, unique=False)
    type = db.Column(db.Enum('Percentage', 'Fixed'))
    price_or_percent = db.Column(db.Float(), nullable=False, unique=False)


"""
DATABASE DOCS:


FOR FUCK SAKE
First Create Invoice,
Before u add it or commit every little shit
U need to append InvoiceItems and InvoiceOtherCosts as an Object
Example =>

inv = Invoice(date=datetime.today().date(), number=1, terms='', company_name='Alireza', customer_name='Someone')
inv.items.append(InvoiceItems(type='Product', name='Dildo', price=5000.00, cycle_or_quantity=1))
db.session.add(inv)
db.session.commit()
"""
